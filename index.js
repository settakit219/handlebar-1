// const express = require('express');
// const app = express();
// const port = 3000;

// const bodyparser = require('body-parser');

// app.use(bodyparser.urlencoded({
//     extended: true
// }));

// //Loads the handlebars module
// const handlebars = require('express-handlebars');

// //instead of app.set('view engine', 'handlebars');
// app.use(bodyparser.json());
// app.set('view engine', 'hbs')

// //instead of app.engine('handlebars', handlebars({
// app.engine('hbs', handlebars({
//     layoutsDir: __dirname + '/views/layouts',
//     //new configuration parameter
//     extname: 'hbs',
//     partialsDir: './views/partials'
// }));

// app.use(express.static('public'))

// app.get('/', (req, res) => { 
//     res.render('main', {layout : 'index'});
// });

// app.get('/sec', (req, res) => { 
//     res.render('secondary', {layout : 'index'});
// });

// app.get('/api/login', (req, res) => { 
//     console.log("login");
// });

// app.get('/login', (req, res) => { 
//     res.render('login', { layout: 'login' });
// });

// app.listen(port, () => console.log(`App listening to port ${port}`));

const express = require('express');
const path = require('path');
const exphbs = require('express-handlebars');

const webController = require('./router/webController');
const apiController = require('./router/apiController');

var app = express();
app.use(express.urlencoded({extended: true}));

// file static path
// app.use('/static', express.static(path.join(__dirname, 'public/')));
app.use(express.static('public'));

app.use(express.json());
app.set('views', path.join(__dirname, '/views/'));
app.engine('hbs', exphbs({ extname: 'hbs', defaultLayout: 'index', layoutsDir: __dirname + '/views/layouts/' }));
app.set('view engine', 'hbs');

app.listen(3000, () => {
    console.log('SERVER is listening on PORT 3000');
});

app.use('/', webController);
app.use('/api', apiController);