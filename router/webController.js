const express = require('express');
const carbone = require('carbone');
const fs = require('fs');

const webController = express.Router();

webController.get('/', (req, res) => { 
    res.render('main', {layout : 'index'});
});

webController.get('/sec', (req, res) => { 
    res.render('secondary', {layout : 'index'});
});

webController.get('/login', (req, res) => { 
    res.render('login', { layout: 'login' });
});

webController.get('/report/:fileType', (req, res) => {

    let convertTo = "pdf"

    const fileType = req.params.fileType;

    if (fileType !== "pdf") {
        convertTo = ""
    }

    const opt  = {
        convertTo: convertTo
    };

    var data = [
        {
            movieName: "test",
            actors: [
                {
                    firstname : 'John',
                    lastname : 'Doe'
                },
                {
                    firstname : 'John',
                    lastname : 'Doe'
                }
            ]
        },
        {
            movieName: "test",
            actors: [
                {
                    firstname : 'John',
                    lastname : 'Doe'
                },
                {
                    firstname : 'John',
                    lastname : 'Doe'
                }
            ]
        },
        {
            movieName: "test",
            actors: [
                {
                    firstname : 'John',
                    lastname : 'Doe'
                },
                {
                    firstname : 'John',
                    lastname : 'Doe'
                }
            ]
        },
        {
            movieName: "test",
            actors: [
                {
                    firstname : 'John',
                    lastname : 'Doe'
                },
                {
                    firstname : 'John',
                    lastname : 'Doe'
                }
            ]
        },
        {
            movieName: "test",
            actors: [
                {
                    firstname : 'John',
                    lastname : 'Doe'
                },
                {
                    firstname : 'John',
                    lastname : 'Doe'
                }
            ]
        },
        {
            movieName: "test",
            actors: [
                {
                    firstname : 'John',
                    lastname : 'Doe'
                },
                {
                    firstname : 'John',
                    lastname : 'Doe'
                }
            ]
        },
        {
            movieName: "test",
            actors: [
                {
                    firstname : 'John',
                    lastname : 'Doe'
                },
                {
                    firstname : 'John',
                    lastname : 'Doe'
                }
            ]
        }
        
    ];
    carbone.render('./node_modules/carbone/examples/movies.docx', data, opt, function(err, result){
        if (err) return console.log(err);
        fs.writeFileSync('result.jpg', result);
        res.contentType(`image/jpg`);
        res.send(result);
        // res.send(Buffer.from(result)).status(200).json({ 'status': 'ok' })
        // res.send(200)
        // process.exit(); // to kill automatically LibreOffice workers
    });
})

module.exports = webController;