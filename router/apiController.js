const express = require('express');
const apiController = express.Router();

apiController.post('/login', (req, res) => { 
    const username = req.body.username;
    const password = req.body.password;
    if (username == "" && password == "") {
        res.redirect('/login')
        return;
    }
    if (username !== "admin" && password !== "P@ssw0rd") {
        res.redirect('/login')
        return;
    }
    res.redirect('/')
});

module.exports = apiController;